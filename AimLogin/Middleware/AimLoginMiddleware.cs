﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AimLogin.Misc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AimLogin.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using AimLogin.DbModel;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Aim.DataMapper;

namespace AimLogin.Middleware
{
    /// <summary>
    /// Event arg object for the `OnAddUserClaims` event in `AimLoginMiddlewareConfig`.
    /// </summary>
    public class AimLoginAddUserClaimsEventArgs
    {
        /// <summary>
        /// The principal representing the user.
        /// </summary>
        public ClaimsPrincipal userPrincipal;

        /// <summary>
        /// The database entity representing the user.
        /// </summary>
        public User userDb;

        /// <summary>
        /// The data map manager, which is what you can use to CRUD data about the user.
        /// </summary>
        public DataMapService<User> dataMap;
    }

    /// <summary>
    /// The configuration object for the `AimLoginMiddleware`
    /// </summary>
    public class AimLoginMiddlewareConfig
    {
        /// <summary>
        /// Every request, after a logged in user has been processed by the middleware (including
        /// to check if their AimLogin user id is valid), this function is called so user code can
        /// attach any additional claims onto the user principal.
        /// </summary>
        public event EventHandler<AimLoginAddUserClaimsEventArgs> OnAddUserClaims;

        public void ExecuteOnAddUserClaims(object sender, AimLoginAddUserClaimsEventArgs args)
        {
            this.OnAddUserClaims(sender, args);
        }
    }

    /// <summary>
    /// This middleware is responsible for several things:
    /// 
    /// 1. If the user is authenticated, this middleware will ensure that the user
    ///    has a session id, a valid session, and a valid user object in the database.
    ///    Any issue with any of these points will make the middleware force the user to sign out.
    ///    
    /// 2. It is responsible for adding claims related to the AimLogin onto the user principal.
    ///    It's configuration class also allows user code to subscribe to an event, so the user
    ///    can add claims for their own systems, with the guarentee that the user is running off a valid
    ///    session by the time they get to the claim addition code.
    /// </summary>
    public class AimLoginMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IOptionsMonitor<AimLoginMiddlewareConfig> _config;

        public AimLoginMiddleware(IOptionsMonitor<AimLoginMiddlewareConfig> config, RequestDelegate next)
        {
            _next = next;
            _config = config;
        }

        public async Task InvokeAsync(HttpContext context,
                                      ILogger<AimLoginMiddleware> logger,
                                      IAimUserManager users,
                                      DataMapService<User> maps,
                                      AimLoginContext db)
        {   
            if (context.User.Identity.IsAuthenticated)
            {
                // Get the cookie (if it exists).
                string cookie;
                bool cookieExists = context.Request.Cookies.TryGetValue(AimLoginCookie.CookieName, out cookie);
                if (!cookieExists)
                {
                    logger.LogError($"Connection {context.Connection.Id} is authenticated, however it doesn't have the AimLogin cookie. Forcing Signout");
                    await context.SignOutAsync();
                    context.User = new ClaimsPrincipal(new ClaimsIdentity()); // SignOutAsync doesn't reset IsAuthenticated, so we just make an empty Principal for this request.
                    await this._next(context);
                    return;
                }

                // Parse the cookie.
                var aimCookie = new AimLoginCookie(context.Response.Cookies, context.Request.Cookies);
                var session = aimCookie.SessionToken;

                // Find the session info that's associated to this user's session, and use it as a reverse lookup for the user itself.
                // TODO: This is probably a code smell, so it might be best to move this code into a dedicated/apporpriate class.
                // NOTE: This is essentially a reverse lookup, which *could* be built into the IAimUserDataSingle interface. (GetUserFromData)
                var sessionDb = await db.UserSessionInfos.FirstOrDefaultAsync(i => i.SessionToken == session);
                if (sessionDb == null)
                {
                    logger.LogError($"Connection {context.Connection.Id} is authenticated, has a cookie, but the token is invalid. Forcing Signout");
                    await context.SignOutAsync();
                    context.User = new ClaimsPrincipal(new ClaimsIdentity()); // SignOutAsync doesn't reset IsAuthenticated, so we just make an empty Principal for this request.
                    await this._next(context);
                    return;
                }

                // This *can* fail, but it really, really shouldn't if we get this far.
                var userId = await maps.SingleValue<UserSessionInfo>().GetAllMappingInfo()
                                       .Where(m => m.DataId == sessionDb.UserSessionInfoId)
                                       .Select(m => m.UserId)
                                       .FirstAsync();
                var user = await db.Users.FirstAsync(u => u.UserId == userId);

                await db.FetchUserMappingsFor(user);

                // Add any claims we need by default.
                context.User.AddIdentity(new ClaimsIdentity(new[] { new Claim(AimLoginClaims.UserId, Convert.ToString(user.UserId)) }));

                // Then let the user code add whatever claims they need.
                this._config?.CurrentValue?.ExecuteOnAddUserClaims(this, new AimLoginAddUserClaimsEventArgs
                {
                    dataMap = maps,
                    userDb = user,
                    userPrincipal = context.User
                });
            }

            await this._next(context);
        }
    }
}
