﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace AimLogin.DbModel
{
    public class UserDataMap
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserDataMapId { get; set; }

        [Required]
        public int UserId { get; set; }
        public User User { get; set; }

        public int DataType { get; set; }
        public int TableDataId { get; set; }
    }

    public static class UserDataMapExt
    {
        public async static Task<User> FetchUserMappingsFor(this AimLoginContext db, User user)
        {
            await db.Entry(user).Collection(u => u.UserDataMaps).LoadAsync();
            return user;
        }
    }
}
