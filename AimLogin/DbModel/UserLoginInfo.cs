﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace AimLogin.DbModel
{
    public class UserLoginInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserLoginInfoId { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MaxLength(60)]
        public byte[] PassHash { get; set; }

        [Required]
        public string Salt { get; set; }

        [Required]
        public bool HasEmailBeenVerified { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
