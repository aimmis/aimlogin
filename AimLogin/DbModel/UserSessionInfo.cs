﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace AimLogin.DbModel
{
    public class UserSessionInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserSessionInfoId { get; set; }

        [Required]
        public string SessionToken { get; set; }

        [Required]
        public DateTimeOffset ExpiresUTC { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
