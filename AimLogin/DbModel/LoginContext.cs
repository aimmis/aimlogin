﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AimLogin.DbModel
{
    public class AimLoginContext : DbContext
    {
        public AimLoginContext(DbContextOptions<AimLoginContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder optionsBuilder)
        {
            optionsBuilder.Entity<UserLoginInfo>()
                          .HasIndex(v => v.Username)
                          .IsUnique();

            optionsBuilder.Entity<UserSessionInfo>()
                          .HasIndex(v => v.SessionToken)
                          .IsUnique();

            optionsBuilder.Entity<UserDataMap>()
                          .HasIndex(nameof(UserDataMap.DataType), nameof(UserDataMap.UserId));
            optionsBuilder.Entity<UserDataMap>()
                          .HasIndex(nameof(UserDataMap.DataType), nameof(UserDataMap.TableDataId), nameof(UserDataMap.UserId))
                          .IsUnique();

            optionsBuilder.Entity<UserEmail>()
                          .HasIndex(v => v.Email)
                          .IsUnique();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserDataMap> UserDataMaps { get; set; }
        public DbSet<UserLoginInfo> UserLoginInfos { get; set; }
        public DbSet<UserSessionInfo> UserSessionInfos { get; set; }
        public DbSet<UserEmail> UserEmail { get; set; }
    }
}
