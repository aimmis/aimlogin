﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AimLogin.DbModel;
using BCrypt.Net;
using Microsoft.Extensions.DependencyInjection;
using AimLogin.Misc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Aim.DataMapper;
using Microsoft.Extensions.Options;

namespace AimLogin.Services
{
    /// <summary>
    /// The default user manager provided by this library.
    /// </summary>
    public class AimUserManager : IAimUserManager
    {
        private readonly AimLoginContext                _db;
        private readonly IAimHasher                     _hasher;
        private readonly DataMapService<User>           _dataMap;
        private readonly ILogger<AimUserManager>        _logger;
        private readonly IAimSmtpClient                 _email;
        private readonly IOptions<IAimSmtpDomainConfig> _domains;

        public AimUserManager(AimLoginContext                db, 
                              IAimHasher                     hasher, 
                              DataMapService<User>           dataMap, 
                              ILogger<AimUserManager>        logger,
                              IAimSmtpClient                 email,
                              IOptions<IAimSmtpDomainConfig> domains)
        {
            this._db = db;
            this._hasher = hasher;
            this._dataMap = dataMap;
            this._logger = logger;
            this._email = email;
            this._domains = domains;
        }

        public async Task ChangeUserPassword(User user, string password)
        {
            this._logger.LogInformation($"Changing password for user #{user.UserId}");
            
            var info = await this._dataMap.SingleValue<UserLoginInfo>().GetOrDefaultAsync(user);
            if(info == null)
                throw new Exception("Internal server error: This user apparently doesn't have any login information.");

            info.PassHash = (await this._hasher.HashWithSalt(password, info.Salt)).ToArray();
            await this._dataMap.SingleValue<UserLoginInfo>().SetAsync(user, info);
            await this._email.SendPasswordChangeAlert(user);
        }

        public async Task<User> CreateNewUser(string username, string password, string email)
        {
            this._logger.LogInformation($"Creating new user called '{username}'");

            // Make sure the username is unique
            if(await this._db.UserLoginInfos.AnyAsync(i => i.Username == username)) // Username is an index, so this should be faster than going through data maps
                throw new Exception($"A user with the username {username} already exists.");

            // Same as above, but with email
            if(await this._db.UserEmail.AnyAsync(i => i.Email == email))
                throw new Exception($"A user with the email {email} already exists.");

            // Hash the password.
            string salt = null;
            var hashedPass = await this._hasher.HashWithGeneratedSalt(password, out salt);

            // Create the user, and create the mapping for him.
            var user = new User();
            await this._db.AddAsync(user);
            await this._db.SaveChangesAsync();
            await this._dataMap.SingleValue<UserLoginInfo>().SetAsync(user, new UserLoginInfo
            {
                PassHash = hashedPass.ToArray(),
                Username = username,
                Salt = salt,
                HasEmailBeenVerified = false
            });

            var userEmail = new UserEmail
            {
                Email = email,
                VerifyToken = Guid.NewGuid().ToString()
            };
            await this._dataMap.SingleValue<UserEmail>().SetAsync(user, userEmail);

            // Send a verification email.
            await this._email.SendVerificationEmail(this._domains.Value.VerifyEmailDomain, user, userEmail);

            return user;
        }

        public async Task CreateUserSession(User user, AimLoginCookie cookie)
        {
            this._logger.LogTrace($"Creating new session for User #{user.UserId}");

            var session = await this._dataMap.SingleValue<UserSessionInfo>().GetOrDefaultAsync(user);
            if(session == null)
                session = new UserSessionInfo();

            session.SessionToken = Guid.NewGuid().ToString();
            session.ExpiresUTC   = DateTimeOffset.Now.AddHours(4); // TODO: Make this configurable.

            await this._dataMap.SingleValue<UserSessionInfo>().SetAsync(user, session);
            
            var options = new CookieOptions();
            options.Secure = true;
            options.Expires = session.ExpiresUTC;
            options.IsEssential = true; // You need to be logged in to do anything. You need this cookie to be logged in. I'd say that qualifies.

            cookie.SessionToken = session.SessionToken;
            cookie.SaveChanges(options);
        }

        public async Task<User> GetUserByLogin(string username, string password)
        {
            var info = await this._db.UserLoginInfos
                                     .FirstOrDefaultAsync(u => u.Username == username);

            if(info == null)
                throw new Exception($"User '{username}' does not exist.");

            if(!(await this._hasher.Validate(password, info.Salt, info.PassHash)))
                throw new Exception($"Password mismatch");

            var userId = await this._dataMap.SingleValue<UserLoginInfo>().ReverseLookupUserId(info);
            var user   = await this._db.Users.FirstAsync(u => u.UserId == userId);
            if (!info.HasEmailBeenVerified)
            {
                await this._email.SendVerificationEmail(this._domains.Value.VerifyEmailDomain, 
                                                        user, 
                                                        await this._dataMap.SingleValue<UserEmail>().GetOrDefaultAsync(user));
                throw new EmailNotVerifiedException("Please verify your email, a new verification email has been sent.");
            }
            
            return user;
        }

        public Task<User> PrincipalToUser(ClaimsPrincipal principal)
        {
            return this._db.Users.FirstOrDefaultAsync(u => u.UserId == Convert.ToInt32(principal.FindFirst(AimLoginClaims.UserId)));
        }
    }

    /// <summary>
    /// The main hasher.
    /// </summary>
    public class BCryptAimHasher : IAimHasher
    {
        public Task<IEnumerable<byte>> HashWithGeneratedSalt(string password, out string newSalt)
        {
            newSalt = BCrypt.Net.BCrypt.GenerateSalt();
            return this.HashWithSalt(password, newSalt);
        }

        public async Task<IEnumerable<byte>> HashWithSalt(string password, string salt)
        {
            var hash = await Task.FromResult(BCrypt.Net.BCrypt.HashPassword(password, salt));
            return Encoding.UTF8.GetBytes(hash);
        }

        public async Task<bool> Validate(string password, string salt, IEnumerable<byte> expectedHash)
        {
            return (await this.HashWithSalt(password, salt)).SequenceEqual(expectedHash);
        }
    }


    [Serializable]
    public class EmailNotVerifiedException : Exception
    {
        public EmailNotVerifiedException() { }
        public EmailNotVerifiedException(string message) : base(message) { }
        public EmailNotVerifiedException(string message, Exception inner) : base(message, inner) { }
        protected EmailNotVerifiedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
