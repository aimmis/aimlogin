﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AimLogin.DbModel;
using AimLogin.Misc;
using Microsoft.AspNetCore.Http;

namespace AimLogin.Services
{
    /// <summary>
    /// The interface for the aim user manager.
    /// </summary>
    /// <remarks>
    /// Yes, ASP Core has it's Identity library.
    /// 
    /// No, it doesn't fit in properly with our data mapping system.
    /// 
    /// No, it's not lightweight.
    /// 
    /// No, with the sensitive data we seem to be handling, I'd rather have full control on how it's stored.
    /// </remarks>
    public interface IAimUserManager
    {
        /// <summary>
        /// Converts a user ClaimPrincipal to an actual User object.
        /// </summary>
        /// <param name="principal">The principal to convert.</param>
        /// <returns>
        /// If the given `principal` does not represent a valid, logged in user, then null is returned.
        /// 
        /// Otherwise, the User that the principal represents is returned.
        /// </returns>
        Task<User> PrincipalToUser(ClaimsPrincipal principal);

        /// <summary>
        /// Creates and returns a new User, with the given login information.
        /// </summary>
        /// <remarks>
        /// The login information is stored in the UserLoginInfo data type.
        /// Password is hashed of course.
        /// 
        /// The user cannot login until they've clicked on the verification email. Completion of the verification process
        /// is left up to the website.
        /// </remarks>
        /// <exception cref="Exception">If the given username is already taken.</exception>
        /// <param name="username">The username to give the user.</param>
        /// <param name="password">The non-hashed password to give the user. The manager should take care of hashing it before it's stored.</param>
        /// <param name="email">The email to give the user. This is required for 2FA, among other things.</param>
        /// <returns>The newly created user.</returns>
        Task<User> CreateNewUser(string username, string password, string email);

        /// <summary>
        /// Finds the user with the given login information.
        /// </summary>
        /// <exception cref="Exception">If the user doesn't exist. TODO: This isn't consistant with PrincipalToUser</exception>
        /// <exception cref="Exception">If the password is incorrect.</exception>
        /// <param name="username">The username to give the user.</param>
        /// <param name="password">The non-hashed password to test against.</param>
        /// <returns>The user with the given login information.</returns>
        Task<User> GetUserByLogin(string username, string password);

        /// <summary>
        /// Creates a new session for the user.
        /// </summary>
        /// <remarks>
        /// A user can only be logged in under two conditions:
        ///     - They have a session token stored in their cookies (which this function will create).
        ///         - This means that after calling this function, there are no additional steps to take for making a session.
        ///     - The session in that token is valid (it exists, and it's not expired).
        ///     
        /// Sessions are used to prevent user id impersonation attacks.
        /// 
        /// If we just stored the ID inside a cookie, and tried to use only the id to determine if the user is logged in,
        /// we have a security flaw.
        /// 
        /// All someone has to do is manually edit the cookie to have the Id of their target and then they're suddenly logged into their account.
        /// 
        /// Ids can also be identifying, which likely ties us in with some EU privacy laws.
        /// 
        /// So sessions are used to lockdown certain vulnerabilities:
        ///     - First, sessions are only generated when the user logs in. So only the device that logs into the account
        ///       will be given the session token (which is not easy to guess).
        ///         - The cookie is also set to be secure, so it's only sent over HTTPS, which should mostly make it hard to sniff it from network data.
        ///         - These two facts should mean only people with access to the computers files will be able to get their hands on the token.
        ///             - In the future it might be wise to look into encrypting the token before it's sent as a cookie, since the client
        ///               doesn't actually need it outside of sending it back to us. So all the encryption keys will be kept private, and
        ///               no user should be able to get their hands on it.
        ///     
        ///     - Without access to the database, sessions can't directly identify someone, unlike an ID.
        ///     
        ///     - Sessions are set to expire after a certain amount of time (configurable).
        ///         - An expired session forces the user to have to log in again, so only people with the account info can refresh a session.
        ///         - If someone *did* manage to get hold of a session token for someone else, it wouldn't last long and they'd have
        ///           to get another one after it expires, which, depending on how they got it, can either be a useless measure, or a meaningful one.
        ///             - It also depends on how much damage a malicious person can do with the small amount of time they get.
        ///     
        ///     - There can only be one active session for a user. When a new session is created every other session becomes invalid.
        ///         - This means only one device can be used to log into an account at a time.
        ///             - So if a member of staff is still logged into a computer at Aim (imagine that it's also publically accessable),
        ///               then logs into their phone somewhere else, it'll mean the computer can't be used to access their account now.
        /// </remarks>
        /// <param name="user">The user to create the session for.</param>
        /// <param name="cookie">The AimLoginCookie that allows this function to set the sesion token.</param>
        Task CreateUserSession(User user, AimLoginCookie cookie);

        /// <summary>
        /// Changes the user's password, and sends them an email alert notifying them about this change.
        /// </summary>
        /// <param name="user">The user to change the password of.</param>
        /// <param name="password">The password to give the user.</param
        Task ChangeUserPassword(User user, string password);
    }

    /// <summary>
    /// A password hasher.
    /// 
    /// While it is possible to create your own, just use the default one please (BCryptAimHasher).
    /// </summary>
    public interface IAimHasher
    {
        /// <summary>
        /// Hashes a password with an already existing salt.
        /// </summary>
        /// <param name="password">The non-hashed password.</param>
        /// <param name="salt">The salt to use.</param>
        /// <returns>The hashed password.</returns>
        Task<IEnumerable<byte>> HashWithSalt(string password, string salt);

        /// <summary>
        /// Hashes a password with a newly generated salt.
        /// </summary>
        /// <param name="password">The non-hashed password.</param>
        /// <param name="newSalt">The variable to store the new salt in.</param>
        /// <returns>The hashed password.</returns>
        Task<IEnumerable<byte>> HashWithGeneratedSalt(string password, out string newSalt);

        /// <summary>
        /// Validates the given password to see if it matches another password hash.
        /// </summary>
        /// <param name="password">The non-hashed password to test.</param>
        /// <param name="salt">The salt to hash the non-hashed password with.</param>
        /// <param name="expectedHash">The password hash to test the non-hashed password against (after it's been hashed of course).</param>
        /// <returns>Whether `password` hashed with `salt` is the same hash as `expectedHash`</returns>
        Task<bool> Validate(string password, string salt, IEnumerable<byte> expectedHash);
    }
}
