﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AimLogin.Services
{
    /// <summary>
    /// The different user data types for AimLogin.
    /// </summary>
    public class AimLoginDataTypes
    {
        public static int UserLoginInfo => 1;
        public static int UserSessionInfo => 2;
        public static int UserEmail => 3;
    }
}
