﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Aim.DataMapper;
using AimLogin.DbModel;
using Microsoft.Extensions.Options;

namespace AimLogin.Services
{
    public class AimSmtpClient : IAimSmtpClient
    {
        readonly SmtpClient _smtp;
        readonly DataMapService<User> _data;

        public AimSmtpClient(IOptions<IAimSmtpClientConfig> config, DataMapService<User> data)
        {
            this._smtp = new SmtpClient(config.Value.Host, config.Value.Port)
            {
                Credentials = config.Value.Credentials,
                EnableSsl = true
            };
            this._data = data;
        }

        public async Task SendToAsync(User user, MailMessage message)
        {
            var email = await this._data.SingleValue<UserEmail>().GetOrDefaultAsync(user);
            if(email == null)
                throw new InvalidOperationException($"User #{user.UserId} does not contain a UserEmail mapping.");

            message.To.Add(new MailAddress(email.Email));
            await this._smtp.SendMailAsync(message);
        }
    }
}
