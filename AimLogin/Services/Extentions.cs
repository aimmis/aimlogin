﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AimLogin.DbModel;
using AimLogin.Misc;
using Aim.DataMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AimLogin.Services
{
    /// <summary>
    /// Misc extensions.
    /// </summary>
    public static class AimLoginMiscExtentions
    {
        /// <summary>
        /// A useful extension function that completely adds everything needed for AimLogin to function.
        /// </summary>
        /// <param name="services">The service container to use.</param>
        public static void AddAimLogin(this IServiceCollection services)
        {
            services.AddDefaultUserManager();
            services.AddPasswordHasher();
            services.AddScoped<IAimSmtpClient, AimSmtpClient>();
            new DataMapBuilder<AimLoginContext, AimLoginContext, UserDataMap, AimLoginDataTypes>(services)
                .UseMapDatabase()
                .UseUserType<User>()
                .UseSingleValue<User, UserLoginInfo>()
                .UseSingleValue<User, UserSessionInfo>()
                .UseSingleValue<User, UserEmail>();
        }

        /// <summary>
        /// A helper function to easily sign in using AimLogin.
        /// </summary>
        /// <remarks>
        /// AimLogin requires that Cookie Authentication is configured.
        /// </remarks>
        public static async Task AimSignInAsync(this HttpContext context, User user, IAimUserManager users)
        {
            var cookie = new AimLoginCookie(context);
            await users.CreateUserSession(user, cookie);

            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);
            identity.AddClaim(new Claim("dummy", "value"));

            await context.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity));
        }

        /// <summary>
        /// Adds the specified user manager as a scoped service.
        /// </summary>
        public static void AddUserManager<T>(this IServiceCollection services) where T : class, IAimUserManager
        {
            services.AddScoped<IAimUserManager, T>();
        }

        /// <summary>
        /// Adds the default user manager as a scoped service.
        /// </summary>
        public static void AddDefaultUserManager(this IServiceCollection services)
        {
            services.AddUserManager<AimUserManager>();
        }

        /// <summary>
        /// Adds the default password hasher as a transient service.
        /// </summary>
        public static void AddPasswordHasher(this IServiceCollection services)
        {
            services.AddTransient<IAimHasher, BCryptAimHasher>();
        }
        
        public static Task EnsureUserMapsLoaded(this User user, AimLoginContext db)
        {
            return db.Entry(user).Collection(u => u.UserDataMaps).LoadAsync();
        }

        #region EMAIL HELPERS
        public static Task SendVerificationEmail(this IAimSmtpClient email, string domain, User user, UserEmail userEmail)
        {
            return email.SendToAsync(user, new System.Net.Mail.MailMessage
            {
                IsBodyHtml = true,
                Subject = "Please verify your account.",
                Priority = System.Net.Mail.MailPriority.Normal,
                Body = $"<body>To finish setting up your account, please click this link, or yell at Andy: " +
                       $"{domain}{userEmail.VerifyToken}<br/><br/>" +
                       $"If this is in your spam folder, you'll have to copy and paste the link into your address bar.<br/>" +
                       $"Hopefully Andy will help me stop getting it marked as spam..." +
                       $"</body>",
                BodyEncoding = Encoding.UTF8,
                From = new System.Net.Mail.MailAddress("noreply@aimeducational.co.uk", "AimLogin System")
            });
        }

        public static Task SendPasswordChangeAlert(this IAimSmtpClient email, User user)
        {
            return email.SendToAsync(user, new System.Net.Mail.MailMessage
            {
                IsBodyHtml = true,
                Subject = "[Alert] Your password has been changed.",
                Priority = System.Net.Mail.MailPriority.Normal,
                Body = 
                $"<body>" +
                $"This is just a notice that your password has been changed.<br/><br/>" +
                $"If you were not responsible for this change, please contact Andy ASAP." +
                $"</body>",
                BodyEncoding = Encoding.UTF8
            });
        }
        #endregion
    }
}
