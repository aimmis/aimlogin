﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using AimLogin.DbModel;

namespace AimLogin.Services
{
    public class IAimSmtpClientConfig
    {
        public string Host;
        public int Port;
        public NetworkCredential Credentials;
    }

    public class IAimSmtpDomainConfig
    {
        /// <summary>
        /// NOTE: The verify token for the UserEmail to be verified will be appended onto the end, so the domain should
        /// end with an incomplete query value, .e.g "?token="
        /// </summary>
        public string VerifyEmailDomain;
    }

    public interface IAimSmtpClient
    {
        Task SendToAsync(User user, MailMessage message);
    }
}
