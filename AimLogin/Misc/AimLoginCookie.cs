﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace AimLogin.Misc
{
    /// <summary>
    /// This class is used to easily handle the data of the AimLogin cookie.
    /// </summary>
    public class AimLoginCookie
    {
        /// <summary>
        /// The name of the AimLogin cookie.
        /// </summary>
        public static readonly string CookieName = "AimLogin";

        private readonly IResponseCookies _response;
        private readonly JObject _cookie;

        /// <summary>
        /// It should be noted that a new instance of this class should be created per request,
        /// because of how short lived this constructor's parameters are.
        /// </summary>
        /// <param name="response">This allows the class to write it's cookie.</param>
        /// <param name="request">This allows the class to read the already existing cookie.</param>
        public AimLoginCookie(IResponseCookies response, IRequestCookieCollection request)
        {
            this._response = response;
            this._cookie = JObject.Parse((request.ContainsKey(AimLoginCookie.CookieName)) ? request[AimLoginCookie.CookieName] : "{}");
        }

        /// <summary>
        /// Alternative constructor.
        /// </summary>
        public AimLoginCookie(HttpContext context) : this(context.Response.Cookies, context.Request.Cookies)
        { }

        /// <summary>
        /// Saves any changes made to the cookie.
        /// </summary>
        /// <param name="options">The options about the cookie.</param>
        public void SaveChanges(CookieOptions options)
        {
            this._response.Append(AimLoginCookie.CookieName, this._cookie.ToString(Newtonsoft.Json.Formatting.None), options);
        }

        /// <summary>
        /// Gets or Sets the session token.
        /// 
        /// The session token should be the same as `UserSessionInfo.SessionToken` for the user's current session.
        /// </summary>
        public string SessionToken
        {
            get
            {
                try
                {
                    return this._cookie[AimLoginClaims.SessionToken].Value<string>();
                }
                catch(Exception)
                {
                    return "";
                }
            }

            set
            {
                this._cookie[AimLoginClaims.SessionToken] = value;
            }
        }
    }
}
