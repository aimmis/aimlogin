﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace AimLogin.Misc
{
    /// <summary>
    /// Claims related to AimLogin
    /// 
    /// AimLogin's cookie will contain entries for some of these claims, under the exact same names.
    /// </summary>
    public static class AimLoginClaims
    {
        /// <summary>
        /// The id of the user.
        /// 
        /// This is the same id used in AimLogin's `User` table, and is attached to the user principal to make it
        /// easy for code to get a User object from the user's ClaimPrincipal.
        /// </summary>
        public static readonly string UserId = "AIMLOGIN_UserId";

        /// <summary>
        /// The session token string used to prevent impersonation of another user.
        /// 
        /// For example, without this specific check, all someone would have to do is put a random (or worse, a known) ID
        /// into their cookie, and the system would auto log them in.
        /// 
        /// However, because of this token check, any attacker must know both the ID of the user, as well as their current session token,
        /// which after a small amount of time forces the user to log back on to create a new session token.
        /// 
        /// This is mostly useful in it's cookie form (for authentication), so the user ClaimPrincipal doesn't get updated to include this value.
        /// </summary>
        public static readonly string SessionToken = "AIMLOGIN_SessionToken";
    }
}
