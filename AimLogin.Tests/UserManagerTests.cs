using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using AimLogin.DbModel;
using AimLogin.Services;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AimLogin.Misc;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace AimLogin.Tests
{
    public class UserManagerTests
    {
        IServiceProvider services;

        public UserManagerTests()
        {
            var builder = new ServiceCollection();
            builder.AddEntityFrameworkInMemoryDatabase();
            builder.AddDbContext<AimLoginContext>(o =>
            {
                o.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
                o.UseInMemoryDatabase("test");
            });
            builder.AddLogging();
            builder.AddAimLogin();

            this.services = builder.BuildServiceProvider();
            this.services.GetRequiredService<AimLoginContext>().Database.EnsureCreated();
        }

        [Fact]
        public async System.Threading.Tasks.Task TestUserManagerAndHasher()
        {
            var db = this.services.GetRequiredService<AimLoginContext>();
            var manager = this.services.GetRequiredService<IAimUserManager>();

            const string username = "Piggy";
            const string password = "Run Havoc";

            // Create new user without issue.
            var user = await manager.CreateNewUser(username, password, "piggy@havoc.co");
            Assert.NotEqual(0, user.UserId);

            // Attempt to create user that already exists.
            await Assert.ThrowsAnyAsync<Exception>(() => manager.CreateNewUser(username, "Some other password we don't care about", ""));

            // Attempt to create a user with an already existing email.
            await Assert.ThrowsAnyAsync<Exception>(() => manager.CreateNewUser("ta", "to", "piggy@havoc.co"));

            // Get existing user.
            var user2 = await manager.GetUserByLogin(username, password);
            Assert.Equal(user, user2);

            // Get user that doesn't exist
            await Assert.ThrowsAnyAsync<Exception>(() => manager.GetUserByLogin("Tories", "Austerity for the non-rich"));

            // Get user with wrong password
            await Assert.ThrowsAnyAsync<Exception>(() => manager.GetUserByLogin(username, "'Dont put silly things like politics in code'"));
        }
    }
}
